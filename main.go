package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"

	. "gitlab.com/chrisvittal/golox/lox"
)

// sysexits.h exit code values
const (
	EX_OK       = 0
	EX_USAGE    = 64
	EX_DATAERR  = 65
	EX_SOFTWARE = 70
	EX_IOERR    = 74
)

func main() {
	if len(os.Args) == 1 {
		repl()
	} else if len(os.Args) == 2 {
		runFile(os.Args[1])
	} else {
		fmt.Fprintln(os.Stderr, "Usage: golox [path]")
		os.Exit(EX_USAGE)
	}
}

func repl() {
	var vm Vm = NewVm()
	var stdin *bufio.Reader = bufio.NewReader(os.Stdin)
	for {
		fmt.Print("golox> ")
		line, err := stdin.ReadBytes('\n')
		if err != nil {
			if err == io.EOF {
				println()
				if len(line) == 0 {
					os.Exit(EX_OK)
				}
			} else {
				fmt.Fprintf(os.Stderr, "golox io error: %s\n", err)
				os.Exit(EX_IOERR)
			}
		}
		vm.Interpret(bytes.TrimSpace(line))
	}
}

func runFile(path string) {
	source, err := ioutil.ReadFile(path)
	if err != nil {
		fmt.Fprintf(os.Stderr, "golox io error: %s\n", err)
		os.Exit(EX_IOERR)
	}

	vm := NewVm()
	switch vm.Interpret(source) {
	case INTERPRET_OK:
		os.Exit(EX_OK)
	case INTERPRET_COMPILE_ERROR:
		os.Exit(EX_DATAERR)
	case INTERPRET_RUNTIME_ERROR:
		os.Exit(EX_SOFTWARE)
	}
}
