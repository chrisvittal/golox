
SRCS := $(wildcard **/*.go) $(wildcard *.go)

.PHONY: all

all: golox

# extra generated files here in case they don't exist at makefile variable expansion time
golox: lox/opcode_string.go lox/tokentype_string.go $(SRCS)
	go build

lox/opcode_string.go lox/tokentype_string.go: lox/chunk.go lox/token.go
	cd lox && go generate -x
