package lox

type token struct {
	typ  tokenType
	span []byte
	line int
}

type tokenType uint8

//go:generate $GOPATH/bin/stringer -type=tokenType
const (
	// single char tokens
	tokLeftParen tokenType = iota
	tokRightParen
	tokLeftBrace
	tokRightBrace
	tokComma
	tokDot
	tokMinus
	tokPlus
	tokSemicolon
	tokSlash
	tokStar
	// one or two char tokens
	tokBang
	tokBangEqual
	tokEqual
	tokEqualEqual
	tokGreater
	tokGreaterEqual
	tokLess
	tokLessEqual
	// literals
	tokIdent
	tokString
	tokNumber
	// keywords
	tokAnd
	tokClass
	tokElse
	tokFalse
	tokFun
	tokFor
	tokIf
	tokNil
	tokOr
	tokPrint
	tokReturn
	tokSuper
	tokThis
	tokTrue
	tokVar
	tokWhile
	// meta tokens
	tokError
	tokEof
)
