package lox

import (
	"errors"
	"fmt"
	"os"
	"strconv"
)

func compile(source []byte) (chunk, error) {
	scanner := newScanner(source)
	parser := parser{scanner: scanner, chunk: newChunk()}
	parser.advance()
	parser.expression()
	parser.consume(tokEof, "Expect end of expression")
	parser.emitReturn() // end the compile
	// NOTE Debug
	if !parser.hadError {
		parser.chunk.disassemble("code")
	} else {
		return chunk{}, errors.New("compile error")
	}
	return parser.chunk, nil
}

type parser struct {
	prev, cur token
	chunk     chunk
	scanner   scanner
	hadError  bool
	panicMode bool
}

func (p *parser) advance() {
	p.prev = p.cur
	for {
		p.cur = p.scanner.scan()
		if p.cur.typ != tokError {
			break
		}
		p.errorAtCurrent(p.cur.span)
	}
}

func (p *parser) expression() {
	p.precedence(precAssignment)
}

func (p *parser) unary() {
	opType := p.prev.typ
	p.precedence(precUnary)
	switch opType {
	case tokMinus:
		p.emitOp(OP_NEGATE)
	default:
		panic(fmt.Sprintf("unary operator %s that should have been -", opType.String()))
	}
}

func (p *parser) binary() {
	opType := p.prev.typ

	// TODO get operator func
	_, _, prec := getRule(opType)
	p.precedence(prec.up())

	switch opType {
	case tokPlus:
		p.emitOp(OP_ADD)
	case tokMinus:
		p.emitOp(OP_SUBTRACT)
	case tokStar:
		p.emitOp(OP_MULTIPLY)
	case tokSlash:
		p.emitOp(OP_DIVIDE)
	default:
		panic(fmt.Sprintf("binary operator %s that should have been +, -, *, or /", opType.String()))
	}
}

func (p *parser) grouping() {
	p.expression()
	p.consume(tokRightParen, "Expect ')' after expression")
}

func (p *parser) number() {
	val, err := strconv.ParseFloat(string(p.prev.span), 64)
	if err != nil {
		val = 0
	}
	if err := p.chunk.writeConstant(value(val), p.prev.line); err != nil {
		p.err([]byte(err.Error()))
	}
}

func (p *parser) precedence(prec precedence) {
	p.advance()
	prefix, _, _ := getRule(p.prev.typ)
	if prefix == nil {
		p.strErr("Expect expression.")
		return
	}

	prefix(p)

	for _, _, cprec := getRule(p.cur.typ); prec <= cprec; _, _, cprec = getRule(p.cur.typ) {
		p.advance()
		_, infix, _ := getRule(p.prev.typ)
		infix(p)
	}
}

func (p *parser) consume(typ tokenType, msg string) {
	if p.cur.typ == typ {
		p.advance()
		return
	}

	p.errorAtCurrent([]byte(msg))
}

func (p *parser) errorAt(token token, msg []byte) {
	if p.panicMode {
		return
	}
	p.panicMode = true
	fmt.Fprintf(os.Stderr, "[line %d] Error", token.line)

	if token.typ == tokEof {
		fmt.Fprintf(os.Stderr, " at end")
	} else if token.typ == tokError {
		// nothing
	} else {
		fmt.Fprintf(os.Stderr, " at '%s'", token.span)
	}

	fmt.Fprintf(os.Stderr, ": %s\n", msg)
	p.hadError = true
}

func (p *parser) strErr(s string) {
	p.err([]byte(s))
}

func (p *parser) err(msg []byte) {
	p.errorAt(p.prev, msg)
}

func (p *parser) errorAtCurrent(msg []byte) {
	p.errorAt(p.cur, msg)
}

func (p *parser) emitTwo(op OpCode, b byte) {
	p.chunk.pushOp(op, p.prev.line)
	p.chunk.pushCode(b, p.prev.line)
}

func (p *parser) emitByte(b byte) {
	p.chunk.pushCode(b, p.prev.line)
}

func (p *parser) emitOp(op OpCode) {
	p.chunk.pushOp(op, p.prev.line)
}

func (p *parser) emitReturn() {
	p.emitOp(OP_RETURN)
}

type precedence uint8

const (
	precNone precedence = iota
	precAssignment
	precOr
	precAnd
	precEq
	precComp
	precTerm
	precFactor
	precUnary
	precCall
	precPrimary
)

func (p precedence) up() precedence {
	switch p {
	case precNone:
		return precAssignment
	case precAssignment:
		return precOr
	case precOr:
		return precAnd
	case precAnd:
		return precEq
	case precEq:
		return precComp
	case precComp:
		return precTerm
	case precTerm:
		return precFactor
	case precFactor:
		return precUnary
	case precUnary:
		return precCall
	case precCall:
		return precPrimary
	default:
		return precPrimary
	}
}

type parseRule struct {
	prefix     func(p *parser)
	infix      func(p *parser)
	precedence precedence
}

func getRule(typ tokenType) (prefix func(*parser), infix func(*parser), prec precedence) {
	switch typ {
	case tokLeftParen:
		return (*parser).grouping, nil, precCall
	case tokRightParen:
		return nil, nil, precNone
	case tokLeftBrace:
		return nil, nil, precNone
	case tokRightBrace:
		return nil, nil, precNone
	case tokComma:
		return nil, nil, precNone
	case tokDot:
		return nil, nil, precNone
	case tokMinus:
		return (*parser).unary, (*parser).binary, precTerm
	case tokPlus:
		return nil, (*parser).binary, precTerm
	case tokSemicolon:
		return nil, nil, precNone
	case tokSlash:
		return nil, (*parser).binary, precFactor
	case tokStar:
		return nil, (*parser).binary, precFactor
	case tokBang:
		return nil, nil, precNone
	case tokBangEqual:
		return nil, nil, precEq
	case tokEqual:
		return nil, nil, precNone
	case tokEqualEqual:
		return nil, nil, precEq
	case tokGreater:
		return nil, nil, precComp
	case tokGreaterEqual:
		return nil, nil, precComp
	case tokLess:
		return nil, nil, precComp
	case tokLessEqual:
		return nil, nil, precComp
	case tokIdent:
		return nil, nil, precNone
	case tokString:
		return nil, nil, precNone
	case tokNumber:
		return (*parser).number, nil, precNone
	case tokAnd:
		return nil, nil, precAnd
	case tokClass:
		return nil, nil, precNone
	case tokElse:
		return nil, nil, precNone
	case tokFalse:
		return nil, nil, precNone
	case tokFun:
		return nil, nil, precNone
	case tokFor:
		return nil, nil, precNone
	case tokIf:
		return nil, nil, precNone
	case tokNil:
		return nil, nil, precNone
	case tokOr:
		return nil, nil, precOr
	case tokPrint:
		return nil, nil, precNone
	case tokReturn:
		return nil, nil, precNone
	case tokSuper:
		return nil, nil, precNone
	case tokThis:
		return nil, nil, precNone
	case tokTrue:
		return nil, nil, precNone
	case tokVar:
		return nil, nil, precNone
	case tokWhile:
		return nil, nil, precNone
	case tokError:
		return nil, nil, precNone
	case tokEof:
		return nil, nil, precNone
	}
	panic(fmt.Sprintf("invalid token %d", typ))
}
