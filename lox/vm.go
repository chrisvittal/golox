package lox

import (
	"fmt"
)

type value float64
type InterpretResult int

const (
	INTERPRET_OK InterpretResult = iota
	INTERPRET_COMPILE_ERROR
	INTERPRET_RUNTIME_ERROR
)

type Vm struct {
	chunk chunk
	ip    int
	stack stack
}

func NewVm() Vm {
	return Vm{}
}

func (vm *Vm) Interpret(source []byte) InterpretResult {
	chunk, err := compile(source)
	if err != nil {
		return INTERPRET_COMPILE_ERROR
	}
	vm.chunk = chunk
	vm.ip = 0
	return vm.run()
}

func (stack *stack) binaryOp(op func(value, value) value) {
	b := stack.pop()
	a := stack.pop()
	stack.push(op(a, b))
}

func (vm *Vm) run() InterpretResult {
	var prevLine int = 0
	println("== run VM ==")
	for {
		// NOTE: debugging only
		{
			fmt.Printf("          %v\n", vm.stack.arr)
			_, prevLine = vm.chunk.disassembleInstruction(vm.ip, prevLine)
		}
		inst := OpCode(vm.nextByte())
		switch inst {
		case OP_CONSTANT:
			val := vm.chunk.constants[vm.nextByte()]
			vm.stack.push(val)
		case OP_CONSTANT_LONG:
			code := vm.chunk.code
			idx := int(code[vm.ip]) + int(code[vm.ip+1])>>8 + int(code[vm.ip+2])>>16
			vm.ip = vm.ip + 3
			val := vm.chunk.constants[idx]
			vm.stack.push(val)
		case OP_ADD:
			vm.stack.binaryOp(func(a, b value) value { return a + b })
		case OP_SUBTRACT:
			vm.stack.binaryOp(func(a, b value) value { return a - b })
		case OP_MULTIPLY:
			vm.stack.binaryOp(func(a, b value) value { return a * b })
		case OP_DIVIDE:
			vm.stack.binaryOp(func(a, b value) value { return a / b })
		case OP_NEGATE:
			val := vm.stack.pop()
			vm.stack.push(-val)
		case OP_RETURN:
			val := vm.stack.pop()
			val.Print()
			println()
			return INTERPRET_OK
		}
	}
}

func (vm *Vm) nextByte() byte {
	ret := vm.chunk.code[vm.ip]
	vm.ip = vm.ip + 1
	return ret
}
