package lox

import "bytes"

type scanner struct {
	start  []byte
	offset int
	line   int
}

func newScanner(source []byte) scanner {
	return scanner{start: source, offset: 0, line: 1}
}

func (s *scanner) scan() token {
	s.skipWhitespace()
	s.start = s.start[s.offset:]
	s.offset = 0
	if s.atEnd() {
		return s.makeToken(tokEof)
	}
	c := s.nextChar()
	switch c {
	case '(':
		return s.makeToken(tokLeftParen)
	case ')':
		return s.makeToken(tokRightParen)
	case '{':
		return s.makeToken(tokLeftBrace)
	case '}':
		return s.makeToken(tokRightBrace)
	case ',':
		return s.makeToken(tokComma)
	case '.':
		return s.makeToken(tokDot)
	case '-':
		return s.makeToken(tokMinus)
	case '+':
		return s.makeToken(tokPlus)
	case ';':
		return s.makeToken(tokSemicolon)
	case '/':
		return s.makeToken(tokSlash)
	case '*':
		return s.makeToken(tokStar)
	case '!':
		return s.matchToken('=', tokBangEqual, tokBang)
	case '=':
		return s.matchToken('=', tokEqualEqual, tokEqual)
	case '<':
		return s.matchToken('=', tokLessEqual, tokLess)
	case '>':
		return s.matchToken('=', tokGreaterEqual, tokGreater)
	case '"':
		return s.makeString()
	case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
		return s.makeNumber()
	case '_', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm':
		fallthrough
	case 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z':
		fallthrough
	case 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M':
		fallthrough
	case 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
		return s.makeIdent()
	}
	return s.errorToken("Unexpected character.")
}

func (s *scanner) makeString() token {
	for s.peek() != '"' && !s.atEnd() {
		if s.peek() == '\n' {
			s.line++
		}
		s.nextChar()
	}
	if s.atEnd() {
		return s.errorToken("Unterminated string.")
	}
	s.nextChar()
	return s.makeToken(tokString)
}

func (s *scanner) makeNumber() token {
	for isDigit(s.peek()) {
		s.nextChar()
	}
	if s.peek() == '.' && isDigit(s.peekNext()) {
		s.nextChar()
		for isDigit(s.peek()) {
			s.nextChar()
		}
	}
	return s.makeToken(tokNumber)
}

func (s *scanner) makeIdent() token {
	for c := s.peek(); isAlpha(c) || isDigit(c); c = s.peek() {
		s.nextChar()
	}
	typ := s.getIdentType()
	return s.makeToken(typ)
}

func (s *scanner) getIdentType() tokenType {
	switch span := s.start[:s.offset]; {
	case bytes.Equal([]byte("and"), span):
		return tokAnd
	case bytes.Equal([]byte("class"), span):
		return tokClass
	case bytes.Equal([]byte("else"), span):
		return tokElse
	case bytes.Equal([]byte("false"), span):
		return tokFalse
	case bytes.Equal([]byte("fun"), span):
		return tokFun
	case bytes.Equal([]byte("for"), span):
		return tokFor
	case bytes.Equal([]byte("if"), span):
		return tokIf
	case bytes.Equal([]byte("nil"), span):
		return tokNil
	case bytes.Equal([]byte("or"), span):
		return tokOr
	case bytes.Equal([]byte("print"), span):
		return tokPrint
	case bytes.Equal([]byte("return"), span):
		return tokReturn
	case bytes.Equal([]byte("super"), span):
		return tokSuper
	case bytes.Equal([]byte("this"), span):
		return tokThis
	case bytes.Equal([]byte("true"), span):
		return tokTrue
	case bytes.Equal([]byte("var"), span):
		return tokVar
	case bytes.Equal([]byte("while"), span):
		return tokWhile
	}
	return tokIdent
}

func (s *scanner) skipWhitespace() {
	for {
		switch s.peek() {
		case ' ', '\r', '\t':
			s.nextChar()
		case '\n':
			s.line++
			s.nextChar()
		case '/':
			if s.peekNext() == '/' {
				for s.peek() != '\n' && !s.atEnd() {
					s.nextChar()
				}
			} else {
				return
			}
		default:
			return
		}
	}
}

func (s *scanner) match(expected byte) bool {
	if s.atEnd() {
		return false
	}
	if s.start[s.offset] != expected {
		return false
	}

	s.offset++
	return true
}

func (s *scanner) matchToken(expected byte, yes, no tokenType) token {
	if s.match(expected) {
		return s.makeToken(yes)
	}
	return s.makeToken(no)
}

func (s *scanner) makeToken(typ tokenType) token {
	return token{typ: typ, span: s.start[:s.offset], line: s.line}
}

func (s *scanner) errorToken(msg string) token {
	return token{typ: tokError, span: []byte(msg), line: s.line}
}

func (s *scanner) nextChar() byte {
	s.offset++
	return s.start[s.offset-1]
}

func (s *scanner) peek() byte {
	if s.atEnd() {
		return 0
	}
	return s.start[s.offset]
}

func (s *scanner) peekNext() byte {
	if s.atEnd() {
		return 0
	}
	return s.start[s.offset+1]
}

func (s *scanner) atEnd() bool {
	return len(s.start)-s.offset <= 0
}

func isDigit(b byte) bool {
	return '0' <= b && b <= '9'
}

func isAlpha(b byte) bool {
	return b == '_' || ('a' <= b && b <= 'z') || ('A' <= b && b <= 'Z')
}
