package lox

import (
	"errors"
	"fmt"
)

type OpCode byte

//go:generate $GOPATH/bin/stringer -type=OpCode
const (
	OP_RETURN OpCode = iota
	OP_CONSTANT
	OP_CONSTANT_LONG
	OP_ADD
	OP_SUBTRACT
	OP_MULTIPLY
	OP_DIVIDE
	OP_NEGATE
)

type chunk struct {
	code      []byte
	constants []value
	lines     lineArray
}

type lineArray struct {
	prevLine  int
	firstLine int
	nextByte  int
	array     []lineOffset
}

type lineOffset struct {
	byteOff uint8
	lineOff int8
}

func newChunk() chunk {
	return chunk{
		code:      make([]byte, 0),
		constants: make([]value, 0),
		lines:     lineArray{nextByte: -1},
	}
}

func (chunk *chunk) pushOp(op OpCode, line int) {
	chunk.doLine(line)
	chunk.code = append(chunk.code, byte(op))
}

func (chunk *chunk) pushCode(b byte, line int) {
	chunk.doLine(line)
	chunk.code = append(chunk.code, b)
}

func (chunk *chunk) doLine(line int) {
	var lineOff int = 0
	if chunk.lines.firstLine == 0 {
		chunk.lines.firstLine = line
	} else {
		lineOff = line - chunk.lines.prevLine
	}
	chunk.lines.prevLine = line
	chunk.lines.pushLine(lineOff)
}

func (chunk *chunk) addConstant(val value) int {
	chunk.constants = append(chunk.constants, val)
	return len(chunk.constants) - 1
}

func (chunk *chunk) writeConstant(val value, line int) error {
	c := chunk.addConstant(val)
	if c > 0xffffff {
		return errors.New("Too many constants in one chunk")
	}
	if c < 255 {
		chunk.pushOp(OP_CONSTANT, line)
		chunk.pushCode(byte(c), line)
	} else {
		chunk.pushOp(OP_CONSTANT_LONG, line)
		chunk.pushCode(byte(c), line)
		chunk.pushCode(byte(c>>8), line)
		chunk.pushCode(byte(c>>16), line)
	}
	return nil
}

func (chunk *chunk) getLine(offset int) int {
	addr, line := 0, chunk.lines.firstLine
	for _, lo := range chunk.lines.array {
		addr = addr + int(lo.byteOff)
		if addr > offset {
			break
		}
		line = line + int(lo.lineOff)
	}
	return line
}

func (val *value) Print() {
	fmt.Printf("%g", *val)
}

func (la *lineArray) pushLine(lineOff int) {
	la.nextByte = la.nextByte + 1
	if lineOff == 0 {
		return
	}

	for lineOff != 0 {
		var lo lineOffset
		if la.nextByte > 255 {
			la.nextByte = la.nextByte - 255
			lo = lineOffset{byteOff: 255, lineOff: 0}
		} else {
			tmp := la.nextByte
			la.nextByte = 0
			if lineOff < -128 {
				lineOff = lineOff - (-128)
				lo = lineOffset{byteOff: uint8(tmp), lineOff: -128}
			} else if lineOff > 127 {
				lineOff = lineOff - 127
				lo = lineOffset{byteOff: uint8(tmp), lineOff: 127}
			} else {
				lo = lineOffset{byteOff: uint8(tmp), lineOff: int8(lineOff)}
				lineOff = 0
			}
			la.array = append(la.array, lo)
		}
	}
}
