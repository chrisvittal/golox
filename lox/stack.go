package lox

type stack struct {
	arr []value
}

func newStack() stack {
	return stack{arr: make([]value, 0)}
}

func (s *stack) push(v value) {
	s.arr = append(s.arr, v)
}

func (s *stack) pop() value {
	l := len(s.arr)
	if l == 0 {
		panic("Pop called on empty array")
	}
	ret := s.arr[l-1]
	s.arr = s.arr[:l-1]
	return ret
}
