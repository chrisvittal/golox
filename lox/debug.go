package lox

import "fmt"

func (chunk *chunk) disassemble(name string) {
	fmt.Printf("== %s ==\n", name)

	var line int = 0
	for i := 0; i < len(chunk.code); {
		i, line = chunk.disassembleInstruction(i, line)
	}
}

func (chunk *chunk) disassembleInstruction(offset int, prevLine int) (int, int) {
	fmt.Printf("  %04x ", offset)
	line := chunk.getLine(offset)
	if offset > 0 && line == prevLine {
		fmt.Printf("   | ")
	} else {
		fmt.Printf("%4d ", line)
	}
	inst := OpCode(chunk.code[offset])
	switch inst {
	case OP_CONSTANT:
		return chunk.constInstruction(inst.String(), offset), line
	case OP_CONSTANT_LONG:
		return chunk.constLongInstruction(inst.String(), offset), line
	case OP_ADD, OP_SUBTRACT, OP_MULTIPLY, OP_DIVIDE, OP_NEGATE, OP_RETURN:
		return simpleInstruction(inst.String(), offset), line
	default:
		fmt.Printf("Unknown opcode %d\n", inst)
		return offset + 1, line
	}
}

func (chunk *chunk) constInstruction(name string, offset int) int {
	constant := chunk.code[offset+1]
	fmt.Printf("%-16s %4d '", name, constant)
	chunk.constants[constant].Print()
	println("'")
	return offset + 2
}

func (chunk *chunk) constLongInstruction(name string, offset int) int {
	constant := int(chunk.code[offset+1]) + int(chunk.code[offset+2])>>8 + int(chunk.code[offset+3])>>16
	fmt.Printf("%-16s %4d '", name, constant)
	chunk.constants[constant].Print()
	println("'")
	return offset + 4
}

func simpleInstruction(name string, offset int) int {
	println(name)
	return offset + 1
}
